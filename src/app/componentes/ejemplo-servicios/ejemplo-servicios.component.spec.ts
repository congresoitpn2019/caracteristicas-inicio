import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploServiciosComponent } from './ejemplo-servicios.component';

describe('EjemploServiciosComponent', () => {
  let component: EjemploServiciosComponent;
  let fixture: ComponentFixture<EjemploServiciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploServiciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
