import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploGuardsComponent } from './ejemplo-guards.component';

describe('EjemploGuardsComponent', () => {
  let component: EjemploGuardsComponent;
  let fixture: ComponentFixture<EjemploGuardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploGuardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploGuardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
