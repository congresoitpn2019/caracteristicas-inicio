import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 

import { AppComponent } from './app.component';
import { EjemploDirectivasComponent } from './componentes/ejemplo-directivas/ejemplo-directivas.component';
import { EjemploServiciosComponent } from './componentes/ejemplo-servicios/ejemplo-servicios.component';
import { EjemploGuardsComponent } from './componentes/ejemplo-guards/ejemplo-guards.component';
import { EjemploPipesComponent } from './componentes/ejemplo-pipes/ejemplo-pipes.component';


const appRoutes: Routes = [
  {
    path: '', 
    redirectTo: '/directivas', 
    pathMatch: 'full'
  }, 
  {
    path: 'directivas', 
    component: EjemploDirectivasComponent
  }, 
  {
    path: 'servicios', 
    component: EjemploServiciosComponent
  }, 
  {
    path: 'pipes', 
    component: EjemploPipesComponent
  }, 
  {
    path: 'guards', 
    component: EjemploGuardsComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    EjemploDirectivasComponent,
    EjemploServiciosComponent,
    EjemploGuardsComponent,
    EjemploPipesComponent
  ],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
